RSpec.describe "Potepan::Products", type: :request do
  describe "GET #show" do
    let(:product) { create :product }

    before do
      get potepan_product_path(product.id)
    end

    it "httpリクエストは成功する" do
      expect(response).to have_http_status(:success)
    end
  end
end
