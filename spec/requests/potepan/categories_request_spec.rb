RSpec.describe "Potepan::Categories", type: :request do
  describe "GET #show" do
    let(:taxonomy) { create :taxonomy }
    let(:master_variant) { create :master_variant, images: [create(:image)] }
    let(:other_master_variant) do
      create :master_variant,
             images: [create(:image, attachment_file_name: "other_image.jpg")],
             price: "0.2e0"
    end
    let(:taxon) { create :taxon }
    let(:other_taxon) { create :taxon, name: "other_taxon" }
    let!(:product) { create :product, master: master_variant, taxons: [taxon] }
    let!(:other_product) { create :product, master: other_master_variant, taxons: [other_taxon] }

    before do
      get potepan_category_path(taxon.id)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "taxonomy is included" do
      expect(response.body).to include taxonomy.name
    end

    it "taxon is included" do
      expect(response.body).to include taxon.name
    end

    it "name is included" do
      expect(response.body).to include product.name
    end

    it "other_product's name is not included" do
      expect(response.body).not_to include other_product.name
    end

    it "price is included" do
      expect(response.body).to include product.display_price.to_s
    end

    it "other_product's price is not included" do
      expect(response.body).not_to include other_product.display_price.to_s
    end

    it "image is included" do
      expect(response.body).to include product.display_image.attachment(:large)
    end

    it "other_product's image is not included" do
      expect(response.body).not_to include other_product.display_image.attachment(:large)
    end
  end
end
