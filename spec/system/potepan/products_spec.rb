RSpec.describe 'Potepan::Products', type: :system do
  let(:master_variant) { create :master_variant, images: [create(:image)] }
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create :product, master: master_variant, taxons: [taxon] }
  let(:not_related_product) { create(:product) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before do
    visit potepan_product_path(product)
  end

  it "product/:idページを表示する" do
    expect(page).to have_link('HOME', href: potepan_path, count: 2)
    within ".lightSection" do
      expect(page).to have_content product.name
    end

    within ".mainContent" do
      expect(page).to have_content product.name
      expect(page).to have_content product.description
      expect(page).to have_content product.display_price.to_s
      expect(page).to have_css("img[src='#{product.display_image.attachment(:large)}']")
      expect(page).to have_link("一覧ページへ戻る", href: potepan_category_path(taxon.id))
    end

    within '.productsContent' do
      expect(page).to have_css ".productBox", count: 4
    end
  end

  it "product/:idページに表示しない" do
    within ".mainContent" do
      visit potepan_product_path(not_related_product)
      expect(page).not_to have_content "一覧ページへ戻る"
    end
  end

  it "'一覧ページを戻る'をクリック後、categories/:idページに遷移する" do
    within '.mainContent' do
      click_link('一覧ページへ戻る')
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  it 'related_productsをクリック後、products/:idページに遷移する' do
    within '.productsContent' do
      expect(page).to have_content related_products.first.name
      click_on related_products.first.name
      expect(current_path).to eq potepan_product_path(related_products.first)
    end

    within ".mainContent" do
      expect(page).to have_content related_products.first.name
      expect(page).to have_content related_products.first.description
      expect(page).to have_content related_products.first.display_price.to_s
    end
  end
end
