RSpec.describe 'Potepan::Categories', type: :system do
  let(:taxonomy) { create :taxonomy }
  let(:master_variant) { create :master_variant, images: [create(:image)] }
  let(:other_master_variant) do
    create :master_variant,
           images: [create(:image, attachment_file_name: "other_image.jpg")],
           price: "0.2e0"
  end
  let(:taxon) { create :taxon, parent_id: taxonomy.root.id }
  let(:other_taxon) { create :taxon, parent_id: taxonomy.root.id, name: "other_taxon" }
  let!(:product) { create :product, master: master_variant, taxons: [taxon] }
  let!(:other_product) { create :product, master: other_master_variant, taxons: [other_taxon] }

  before do
    visit potepan_category_path taxon.id
  end

  it 'categories/:idページを表示する' do
    expect(page).to have_link('HOME', href: potepan_path, count: 2)
    within '.productBox' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price.to_s
      expect(page).to have_css "img[src='#{product.display_image.attachment(:large)}']"
      expect(page).not_to have_content other_product.name
      expect(page).not_to have_content other_product.display_price.to_s
      expect(page).not_to have_css "img[src='#{other_product.display_image.attachment(:large)}']"
    end
    within '.sideBar' do
      expect(page).to have_content taxonomy.name
    end
  end

  it 'productをクリック後、products/:idページに遷移する' do
    within '.productBox' do
      expect(page).to have_link(product.name, href: potepan_product_path(product))
      expect(page).to have_link(product.display_price.to_s, href: potepan_product_path(product))
      click_link href: potepan_product_path(product)
      expect(current_path).to eq potepan_product_path(product)
    end
  end

  it 'カテゴリ名をクリック後、categories/:idページに遷移する' do
    within '.sideBar' do
      expect(page).to have_link(taxon.name, href: potepan_category_path(taxon.id))
      click_link href: potepan_category_path(taxon.id)
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end
end
