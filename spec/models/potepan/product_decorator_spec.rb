RSpec.describe 'Spree::ProductDecorator', type: :model do
  describe "related_products" do
    let(:taxon) { create(:taxon) }
    let(:taxon2) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:related_product) { create(:product, taxons: [taxon]) }
    let(:not_related_product) { create(:product) }
    let(:other_related_product) { create(:product, taxons: [taxon2]) }

    it 'メイン商品に関連する商品は含む' do
      expect(product.related_products).to include related_product
    end

    it 'メイン商品に関連しない商品は含まない' do
      expect(product.related_products).not_to include not_related_product
      expect(product.related_products).not_to include other_related_product
    end

    it 'メイン商品を含まない' do
      expect(product.related_products).not_to include product
    end
  end
end
